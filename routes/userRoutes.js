const express = require("express");
const router = express.Router();
const {
  registerUser,
  loginUser,
  getSingleUser,
  updateProfile,
  enroll,
  checkEmailExists,
  getEnrollments,
} = require("../controllers/userControllers");
const { verify } = require("../auth");

router.post("/", registerUser);

router.post("/login", loginUser);

router.get("/getUserDetails", verify, getSingleUser);

router.put("/updateProfile", verify, updateProfile);

router.post("/enroll", verify, enroll);

router.post("/checkEmailExists", checkEmailExists);

router.get("/getEnrollments", verify, getEnrollments);

module.exports = router;
