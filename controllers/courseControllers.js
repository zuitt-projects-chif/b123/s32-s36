const Course = require("../models/Course");

module.exports.addCourse = (req, res) => {
  const { name, description, price } = req.body;
  const doc = new Course({
    name,
    description,
    price,
  });
  doc
    .save()
    .then((savedDoc) => {
      return res.send(savedDoc);
    })
    .catch((e) => res.send(e));
};

module.exports.getAllCourses = (req, res) => {
  Course.find({})
    .then((result) => res.send(result))
    .catch((e) => res.send(e));
};

module.exports.getActiveCourses = (req, res) => {
  Course.find({ isActive: true })
    .then((result) => res.send(result))
    .catch((e) => res.send(e));
};

module.exports.getSingleCourse = (req, res) => {
  Course.findById(req.params.id)
    .then((result) => res.send(result))
    .catch((e) => res.send(e));
};

module.exports.updateSingleCourse = (req, res) => {
  const { name, description, price } = req.body;
  const updates = {
    name,
    description,
    price,
  };
  Course.findByIdAndUpdate(req.params.id, updates, { new: true })
    .then((updatedTask) => res.send(updatedTask))
    .catch((err) => res.send(err));
};

module.exports.archiveSingleCourse = (req, res) => {
  const updates = {
    isActive: false,
  };
  Course.findByIdAndUpdate(req.params.id, updates, { new: true })
    .then((updatedTask) => res.send(updatedTask))
    .catch((err) => res.send(err));
};

module.exports.activateSingleCourse = (req, res) => {
  const updates = {
    isActive: true,
  };
  Course.findByIdAndUpdate(req.params.id, updates, { new: true })
    .then((updatedTask) => res.send(updatedTask))
    .catch((err) => res.send(err));
};

module.exports.getEnrollees = (req, res) => {
  Course.findById(req.params.id)
    .then((result) => res.send(result.enrollees))
    .catch((e) => res.send(e));
};
