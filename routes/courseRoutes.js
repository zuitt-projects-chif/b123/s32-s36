const express = require("express");
const router = express.Router();
const {
  addCourse,
  getAllCourses,
  getActiveCourses,
  getSingleCourse,
  updateSingleCourse,
  activateSingleCourse,
  archiveSingleCourse,
  getEnrollees,
} = require("../controllers/courseControllers");
const { verify, verifyAdmin } = require("../auth");

router.post("/", verify, verifyAdmin, addCourse);

router.get("/", verify, verifyAdmin, getAllCourses);

router.get("/getActiveCourses", getActiveCourses);

router.get("/getSingleCourse/:id", getSingleCourse);

router.put("/:id", verify, verifyAdmin, updateSingleCourse);

router.put("/archive/:id", verify, verifyAdmin, archiveSingleCourse);

router.put("/activate/:id", verify, verifyAdmin, activateSingleCourse);

router.get("/getEnrollees/:id", verify, verifyAdmin, getEnrollees);

module.exports = router;
