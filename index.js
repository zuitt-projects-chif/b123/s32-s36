const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

const app = express();
const port = process.env.PORT || 4000;

mongoose.connect(
  "mongodb+srv://nadchif:8sFt16XPJIjJ4ViG@cluster0.io6rf.mongodb.net/bookingAPI?retryWrites=true&w=majority",
  { useNewUrlParser: true, useUnifiedTopology: true }
);

const db = mongoose.connection;
db.on("error", console.error.bind(console, "DB Connection Error."));
db.once("open", () =>
  console.log(`${new Date().toLocaleTimeString()}| Connected to MongoDB`)
);

app.use(cors());
app.use(express.json());

const courseRoutes = require("./routes/courseRoutes");
app.use("/courses", courseRoutes);
const userRoutes = require("./routes/userRoutes");
app.use("/users", userRoutes);

app.listen(port, () =>
  console.log(
    `${new Date().toLocaleTimeString()}| Server is running on ${port}`
  )
);
