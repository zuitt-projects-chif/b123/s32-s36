const User = require('../models/User');
const Course = require('../models/Course');
const bycrypt = require('bcrypt');
const { createAccessToken } = require('../auth');

module.exports.registerUser = (req, res) => {
  console.log(req.body);
  const { email, password, firstName, lastName, mobileNo } = req.body;
  const hashedPW = bycrypt.hashSync(password, 10);
  if (password.length < 8)
    return res.status(422).send({ message: 'Password is too short' });

  const doc = new User({
    email,
    password: hashedPW,
    firstName,
    lastName,
    mobileNo,
  });
  doc
    .save()
    .then((savedDoc) => {
      return res.send(savedDoc);
    })
    .catch((e) => res.send(e));
};

module.exports.loginUser = (req, res) => {
  const { email, password } = req.body;
  User.findOne({ email })
    .then((result) => {
      if (result === null) {
        return res.status(401).send({ message: 'No User Found.' });
      }
      console.log(email, password);
      const isPasswordCorrect = bycrypt.compareSync(password, result.password);
      if (isPasswordCorrect) {
        return res.send({ token: createAccessToken(result) });
      } else {
        return res.send({ message: 'Password is incorrect' });
      }
    })
    .catch((e) => res.send(e));
};

module.exports.getSingleUser = (req, res) => {
  const { id } = req.user;
  User.findById(id, { password: 0 })
    .then((result) => res.send(result))
    .catch((e) => res.send(e));
};

module.exports.updateProfile = (req, res) => {
  const { firstName, lastName, mobileNo } = req.user;
  const updates = {
    firstName,
    lastName,
    mobileNo,
  };
  User.findByIdAndUpdate(id, updates, { new: true })
    .then((result) => res.send(result))
    .catch((e) => res.send(e));
};

module.exports.enroll = async (req, res) => {
  if (req.user.isAdmin) {
    return res.status(403).send('Action Forbidden');
  }

  const isUserUpdated = await User.findById(req.user.id).then((user) => {
    user.enrollments.push({ courseId: req.body.courseId });
    return user
      .save()
      .then((user) => true)
      .catch((err) => err.message);
  });
  if (isUserUpdated !== true) {
    return res.send({ message: isUserUpdated });
  }

  const isCourseUpdated = await Course.findById(req.body.courseId).then(
    (course) => {
      course.enrollees.push({ userId: req.user.id });
      return course
        .save()
        .then((course) => true)
        .catch((err) => err.message);
    }
  );
  if (isCourseUpdated !== true) {
    return res.send(isUserUpdated);
  }
  if (isUserUpdated && isCourseUpdated) {
    return res.send({ enrolled: true, message: 'Enrolled Successfully' });
  }
};

module.exports.checkEmailExists = (req, res) => {
  User.findOne({ email: req.body.email }).then((result) => {
    if (result === null) {
      return res.send({ message: 'Email Available.' });
    } else {
      return res.send({ message: 'Email is already registered' });
    }
  });
};

module.exports.getEnrollments = (req, res) => {
  const { id } = req.user;
  User.findById(id)
    .then((result) => res.send(result.enrollments))
    .catch((e) => res.send(e));
};
